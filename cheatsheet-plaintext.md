---
no_site_title: true
title: "Preserves Quick Reference (Plaintext)"
---

Tony Garnock-Jones <tonyg@leastfixedpoint.com>  
{{ site.version_date }}. Version {{ site.version }}.

These are non-normative "reference card" definitions. See also the normative [semantics]({{
site.baseurl }}/preserves.html), [text syntax specification]({{ site.baseurl
}}/preserves-text.html), and [machine-oriented syntax specification]({{ site.baseurl
}}/preserves-binary.html), and the experimental [P-expressions definition]({{ site.baseurl
}}/preserves-expressions.html).

## <a id="binary"></a>Machine-Oriented Binary Syntax

{% include cheatsheet-binary-plaintext.md %}

## <a id="text"></a>Human-Oriented Text Syntax

{% include cheatsheet-text-plaintext.md %}

## <a id="pexprs"></a>P-expression Syntax

{% include cheatsheet-pexprs-plaintext.md %}

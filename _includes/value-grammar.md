```text
                      Value = Atom
                            | Compound
                            | Embedded

                       Atom = Boolean
                            | Double
                            | SignedInteger
                            | String
                            | ByteString
                            | Symbol

                   Compound = Record
                            | Sequence
                            | Set
                            | Dictionary
```

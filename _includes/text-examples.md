Here are a few example values, written using the [text
syntax](https://preserves.dev/preserves-text.html):

    Boolean    : #t #f
    Double     : 1.0 10.4e3 -100.6
    Integer    : 1 0 -100
    String     : "Hello, world!\n"
    ByteString : #"bin\x00str\x00" #[YmluAHN0cgA] #x"62696e0073747200"
    Symbol     : hello-world 'hello world' = ! hello? '' | ...
    Record     : <label field1 field2 ...>
    Sequence   : [value1 value2 ...]
    Set        : #{value1 value2 ...}
    Dictionary : {key1: value1 key2: value2 ...: ...}
    Embedded   : #:value

Commas are optional in sequences, sets, and dictionaries.

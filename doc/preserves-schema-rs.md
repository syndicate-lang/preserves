---
title: preserves-schema-rs
---

The `preserves-schema-rs` program reads
[Preserves Schema](../preserves-schema.html) AST input files (such as
are produced by [`preserves-schemac`]({% link doc/preserves-schemac.md
%})). It produces a collection of Rust source files providing parsers,
unparsers, and Rust data structures reflecting the definitions in the
inputs.

## Using the compiler from `build.rs` instead

You will usually not need to use the `preserves-schema-rs`
command-line program. Instead, access the preserves-schema compiler
API from your `build.rs`. The following example is taken from
[`build.rs` for the `preserves-path` crate](https://gitlab.com/preserves/preserves/-/blob/af5de5b836ffc51999db93797d1995ff677cf6f8/implementations/rust/preserves-path/build.rs):

    use preserves_schema::compiler::*;

    use std::io::Error;
    use std::path::PathBuf;

    fn main() -> Result<(), Error> {
        let buildroot = PathBuf::from(std::env::var_os("OUT_DIR").unwrap());

        let mut gen_dir = buildroot.clone();
        gen_dir.push("src/schemas");

        let mut c = CompilerConfig::new(gen_dir, "crate::schemas".to_owned());

        let inputs = expand_inputs(&vec!["path.bin".to_owned()])?;
        c.load_schemas_and_bundles(&inputs, &vec![])?;

        compile(&c)
    }

This approach also requires an `include!` from your main, hand-written
source tree. The following is a snippet from
[`preserves-path/src/lib.rs`](https://gitlab.com/preserves/preserves/-/blob/af5de5b836ffc51999db93797d1995ff677cf6f8/implementations/rust/preserves-path/src/lib.rs):

    pub mod schemas {
        include!(concat!(env!("OUT_DIR"), "/src/schemas/mod.rs"));
    }

## Installation

The tool is
[written in Rust](https://crates.io/crates/preserves-schema).
[Install `cargo`.](https://doc.rust-lang.org/cargo/getting-started/installation.html)
Then, `cargo install preserves-schema`.

## Usage

    preserves-schema 3.990.2

    USAGE:
        preserves-schema-rs [FLAGS] [OPTIONS] --output-dir <output-dir> --prefix <prefix>
                            [--] [input-glob]...

    FLAGS:
        -h, --help            Prints help information
            --rustfmt-skip
        -V, --version         Prints version information

    OPTIONS:
            --module <module>...
        -o, --output-dir <output-dir>
        -p, --prefix <prefix>
            --support-crate <support-crate>
            --xref <xref>...

    ARGS:
        <input-glob>...

import terser from '@rollup/plugin-terser';

const distfile = (insertion) =>  `dist/preserves${insertion}.js`;

function umd(insertion, extra) {
  return {
    file: distfile(insertion),
    format: 'umd',
    name: 'Preserves',
    ... (extra || {})
  };
}

function es6(insertion, extra) {
  return {
    file: distfile('.es6' + insertion),
    format: 'es',
    ... (extra || {})
  };
}

export default [{
  input: 'lib/index.js',
  output: [
    umd(''),
    umd('.min', { plugins: [terser()] }),
    es6(''),
    es6('.min', { plugins: [terser()] }),
  ],
}];

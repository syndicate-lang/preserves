import type { Compound, Value } from "./values";
import type { Embeddable, GenericEmbedded } from "./embedded";
import { Dictionary, Set } from "./dictionary";

export function isCompound<T extends Embeddable = GenericEmbedded>(x: Value<T>): x is Compound<T>
{
    return (Array.isArray(x) || Set.isSet(x) || Dictionary.isDictionary(x));
}

export function isSequence<T extends Embeddable = GenericEmbedded>(x: Value<T>): x is Array<Value<T>> {
    return (Array.isArray(x) && !('label' in x));
}

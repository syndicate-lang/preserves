import { Annotated } from "./annotated";
import { DecodeError, ShortPacket } from "./codec";
import { Tag } from "./constants";
import { Set, Dictionary, DictionaryMap } from "./dictionary";
import { DoubleFloat } from "./float";
import { Record } from "./record";
import { Bytes, BytesLike, underlying, hexDigit } from "./bytes";
import { Value } from "./values";
import { is } from "./is";
import { GenericEmbedded, Embeddable, EmbeddedTypeDecode } from "./embedded";
import { ReaderStateOptions } from "./reader";
import { stringify } from "./text";

export interface DecoderOptions {
    includeAnnotations?: boolean;
}

export interface DecoderEmbeddedOptions<T extends Embeddable> extends DecoderOptions {
    embeddedDecode?: EmbeddedTypeDecode<T>;
}

export interface TypedDecoder<T extends Embeddable> {
    atEnd(): boolean;

    mark(): any;
    restoreMark(m: any): undefined;

    skip(): void;
    next(): Value<T>;
    withEmbeddedDecode<S extends Embeddable, R>(
        embeddedDecode: EmbeddedTypeDecode<S>,
        body: (d: TypedDecoder<S>) => R): R;

    nextBoolean(): boolean | undefined;
    nextDouble(): DoubleFloat | undefined;
    nextEmbedded(): T | undefined;
    nextSignedInteger(): number | bigint | undefined;
    nextString(): string | undefined;
    nextByteString(): Bytes | undefined;
    nextSymbol(): symbol | undefined;

    openRecord(): boolean;
    openSequence(): boolean;
    openSet(): boolean;
    openDictionary(): boolean;

    closeCompound(): boolean;
}

export function asLiteral<T extends Embeddable, E extends Exclude<Value<T>, Annotated<T>>>(
    actual: Value<T>,
    expected: E): E | undefined
{
    return is(actual, expected) ? expected : void 0;
}

export class DecoderState {
    packet: Uint8Array;
    index = 0;
    options: DecoderOptions;

    constructor(packet: BytesLike, options: DecoderOptions) {
        this.packet = underlying(packet);
        this.options = options;
    }

    get includeAnnotations(): boolean {
        return this.options.includeAnnotations ?? false;
    }

    write(data: BytesLike) {
        if (this.index === this.packet.length) {
            this.packet = underlying(data);
        } else {
            this.packet = Bytes.concat([this.packet.slice(this.index), data])._view;
        }
        this.index = 0;
    }

    atEnd(): boolean {
        return this.index >= this.packet.length;
    }

    mark(): number {
        return this.index;
    }

    restoreMark(m: number): undefined {
        this.index = m;
        return void 0;
    }

    shortGuard<R>(body: () => R, short: () => R): R {
        if (this.atEnd()) return short();
        // ^ important somewhat-common case optimization - avoid the exception

        const start = this.mark();
        try {
            return body();
        } catch (e) {
            if (ShortPacket.isShortPacket(e)) {
                this.restoreMark(start);
                return short();
            }
            throw e;
        }
    }

    nextbyte(): number {
        if (this.atEnd()) throw new ShortPacket("Short packet");
        return this.packet[this.index++];
    }

    nextbytes(n: number): DataView {
        const start = this.index;
        this.index += n;
        if (this.index > this.packet.length) throw new ShortPacket("Short packet");
        // ^ NOTE: greater-than, not greater-than-or-equal-to - this makes atEnd() inappropriate
        return new DataView(this.packet.buffer, this.packet.byteOffset + start, n);
    }

    varint(): number {
        // TODO: Bignums :-/
        const v = this.nextbyte();
        if (v < 128) return v;
        return (this.varint() << 7) + (v - 128);
    }

    peekend(): boolean {
        return (this.nextbyte() === Tag.End) || (this.index--, false);
    }

    nextint(n: number): number | bigint {
        const start = this.index;
        if (n === 0) return 0;
        if (n > 7) return this.nextbigint(n);
        if (n === 7) {
            const highByte = this.packet[this.index];
            if ((highByte >= 0x20) && (highByte < 0xe0)) {
                return this.nextbigint(n);
            }
            // if highByte is 0xe0, we still might have a value
            // equal to (Number.MIN_SAFE_INTEGER-1).
        }
        let acc = this.nextbyte();
        if (acc & 0x80) acc -= 256;
        for (let i = 1; i < n; i++) acc = (acc * 256) + this.nextbyte();
        if (!Number.isSafeInteger(acc)) {
            this.index = start;
            return this.nextbigint(n);
        }
        return acc;
    }

    nextbigint(n: number): bigint {
        if (n === 0) return BigInt(0);
        const bs = Bytes.from(this.nextbytes(n));
        if (bs.get(0) >= 128) {
            // negative
            const hex = bs.toHex(d => hexDigit(15 - d));
            return ~BigInt('0x' + hex);
        } else {
            // (strictly) positive
            const hex = bs.toHex();
            return BigInt('0x' + hex);
        }
    }

    wrap<T extends Embeddable>(v: Value<T>): Value<T> {
        return this.includeAnnotations ? new Annotated(v) : v;
    }

    unshiftAnnotation<T extends Embeddable>(a: Value<T>, v: Annotated<T>): Annotated<T> {
        if (this.includeAnnotations) {
            v.annotations.unshift(a);
        }
        return v;
    }
}

export const neverEmbeddedTypeDecode: EmbeddedTypeDecode<never> = {
    decode(_s: DecoderState): never {
        throw new Error("Embeddeds not permitted at this point in Preserves document");
    },

    fromValue(_v: Value<GenericEmbedded>, _options: ReaderStateOptions): never {
        throw new Error("Embeddeds not permitted at this point in Preserves document");
    },
};

export class Decoder<T extends Embeddable = never> implements TypedDecoder<T> {
    state: DecoderState;
    embeddedDecode: EmbeddedTypeDecode<T>;

    constructor(state: DecoderState, embeddedDecode?: EmbeddedTypeDecode<T>);
    constructor(packet?: BytesLike, options?: DecoderEmbeddedOptions<T>);
    constructor(
        packet_or_state: (DecoderState | BytesLike) = new Uint8Array(0),
        options_or_embeddedDecode?: (DecoderEmbeddedOptions<T> | EmbeddedTypeDecode<T>))
    {
        if (packet_or_state instanceof DecoderState) {
            this.state = packet_or_state;
            this.embeddedDecode = (options_or_embeddedDecode as EmbeddedTypeDecode<T>) ?? neverEmbeddedTypeDecode;
        } else {
            const options = (options_or_embeddedDecode as DecoderEmbeddedOptions<T>) ?? {};
            this.state = new DecoderState(packet_or_state, options);
            this.embeddedDecode = options.embeddedDecode ?? neverEmbeddedTypeDecode;
        }
    }

    write(data: BytesLike) {
        this.state.write(data);
    }

    nextvalues(): Value<T>[] {
        const result = [];
        while (!this.state.peekend()) result.push(this.next());
        return result;
    }

    static dictionaryFromArray<T extends Embeddable>(vs: Value<T>[]): Dictionary<T> {
        const d = new DictionaryMap<T>();
        if (vs.length % 2) throw new DecodeError("Missing dictionary value");
        for (let i = 0; i < vs.length; i += 2) {
            if (d.has(vs[i])) throw new DecodeError(`Duplicate key: ${stringify(vs[i])}`);
            d.set(vs[i], vs[i+1]);
        }
        return d.simplifiedValue();
    }

    next(): Value<T> {
        const tag = this.state.nextbyte();
        switch (tag) {
            case Tag.False: return this.state.wrap<T>(false);
            case Tag.True: return this.state.wrap<T>(true);
            case Tag.End: throw new DecodeError("Unexpected Compound end marker");
            case Tag.Annotation: {
                const a = this.next();
                const v = this.next() as Annotated<T>;
                return this.state.unshiftAnnotation(a, v);
            }
            case Tag.Embedded: return this.state.wrap<T>(this.embeddedDecode.decode(this.state));
            case Tag.Ieee754:
                switch (this.state.varint()) {
                    case 8: return this.state.wrap<T>(DoubleFloat.fromBytes(this.state.nextbytes(8)));
                    default: throw new DecodeError("Invalid IEEE754 size");
                }
            case Tag.SignedInteger: return this.state.wrap<T>(this.state.nextint(this.state.varint()));
            case Tag.String: return this.state.wrap<T>(Bytes.from(this.state.nextbytes(this.state.varint())).fromUtf8());
            case Tag.ByteString: return this.state.wrap<T>(Bytes.from(this.state.nextbytes(this.state.varint())));
            case Tag.Symbol: return this.state.wrap<T>(Symbol.for(Bytes.from(this.state.nextbytes(this.state.varint())).fromUtf8()));
            case Tag.Record: {
                const vs = this.nextvalues();
                if (vs.length === 0) throw new DecodeError("Too few elements in encoded record");
                return this.state.wrap<T>(Record(vs[0], vs.slice(1)));
            }
            case Tag.Sequence: return this.state.wrap<T>(this.nextvalues());
            case Tag.Set: {
                const s = new Set<T>();
                for (const v of this.nextvalues()) {
                    if (s.has(v)) throw new DecodeError(`Duplicate value: ${stringify(v)}`);
                    s.add(v);
                }
                return this.state.wrap<T>(s);
            }
            case Tag.Dictionary: return this.state.wrap<T>(Decoder.dictionaryFromArray(this.nextvalues()));
            default: throw new DecodeError("Unsupported Preserves tag: " + tag);
        }
    }

    try_next(): Value<T> | undefined {
        return this.state.shortGuard(() => this.next(), () => void 0);
    }

    atEnd(): boolean {
        return this.state.atEnd();
    }

    mark(): any {
        return this.state.mark();
    }

    restoreMark(m: any): undefined {
        return this.state.restoreMark(m);
    }

    skip(): void {
        // TODO: be more efficient
        this.next();
    }

    withEmbeddedDecode<S extends Embeddable, R>(
        embeddedDecode: EmbeddedTypeDecode<S>,
        body: (d: TypedDecoder<S>) => R): R
    {
        return body(new Decoder(this.state, embeddedDecode));
    }

    skipAnnotations<R>(f: (reset: () => undefined) => R): R {
        const m = this.mark();
        while (!this.state.atEnd() && this.state.packet[this.state.index] === Tag.Annotation) {
            this.state.index++;
            this.skip();
        }
        return f(() => this.restoreMark(m));
    }

    nextBoolean(): boolean | undefined {
        return this.skipAnnotations((reset) => {
            switch (this.state.nextbyte()) {
                case Tag.False: return false;
                case Tag.True: return true;
                default: return reset();
            }
        });
    }

    nextDouble(): DoubleFloat | undefined {
        return this.skipAnnotations((reset) => {
            if (this.state.nextbyte() !== Tag.Ieee754) return reset();
            if (this.state.nextbyte() !== 8) return reset();
            return DoubleFloat.fromBytes(this.state.nextbytes(8));
        });
    }

    nextEmbedded(): T | undefined {
        return this.skipAnnotations((reset) => {
            switch (this.state.nextbyte()) {
                case Tag.Embedded: return this.embeddedDecode.decode(this.state);
                default: return reset();
            }
        });
    }

    nextSignedInteger(): number | bigint | undefined {
        return this.skipAnnotations((reset) => {
            switch (this.state.nextbyte()) {
                case Tag.SignedInteger: return this.state.nextint(this.state.varint());
                default: return reset();
            }
        });
    }

    nextString(): string | undefined {
        return this.skipAnnotations((reset) => {
            switch (this.state.nextbyte()) {
                case Tag.String: return Bytes.from(this.state.nextbytes(this.state.varint())).fromUtf8();
                default: return reset();
            }
        });
    }

    nextByteString(): Bytes | undefined {
        return this.skipAnnotations((reset) => {
            switch (this.state.nextbyte()) {
                case Tag.ByteString: return Bytes.from(this.state.nextbytes(this.state.varint()));
                default: return reset();
            }
        });
    }

    nextSymbol(): symbol | undefined {
        return this.skipAnnotations((reset) => {
            switch (this.state.nextbyte()) {
                case Tag.Symbol:
                    return Symbol.for(Bytes.from(this.state.nextbytes(this.state.varint())).fromUtf8());
                default:
                    return reset();
            }
        });
    }

    openRecord(): boolean {
        return this.skipAnnotations((reset) =>
            (this.state.nextbyte() === Tag.Record) || (reset(), false));
    }

    openSequence(): boolean {
        return this.skipAnnotations((reset) =>
            (this.state.nextbyte() === Tag.Sequence) || (reset(), false));
    }

    openSet(): boolean {
        return this.skipAnnotations((reset) =>
            (this.state.nextbyte() === Tag.Set) || (reset(), false));
    }

    openDictionary(): boolean {
        return this.skipAnnotations((reset) =>
            (this.state.nextbyte() === Tag.Dictionary) || (reset(), false));
    }

    closeCompound(): boolean {
        return this.state.peekend();
    }
}

export function decode<T extends Embeddable>(
    bs: BytesLike,
    options: DecoderEmbeddedOptions<T> = {},
): Value<T> {
    return new Decoder(bs, options).next();
}

export function decodeWithAnnotations<T extends Embeddable>(
    bs: BytesLike,
    options: DecoderEmbeddedOptions<T> = {},
): Annotated<T> {
    return decode(bs, { ... options, includeAnnotations: true }) as Annotated<T>;
}

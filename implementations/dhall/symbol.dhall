{-|
Create a Preserves symbol from a `Text` value
-}
let Preserves/Type = ./Type.dhall

let Preserves/function = ./function.dhall

let symbol
    : Text → Preserves/Type
    = λ(x : Text) →
      λ(Preserves : Type) →
      λ(value : Preserves/function Preserves) →
        value.symbol x

in  symbol

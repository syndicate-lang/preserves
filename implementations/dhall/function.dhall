λ(Preserves : Type) →
  { boolean : Bool → Preserves
  , double : Double → Preserves
  , integer : Integer → Preserves
  , string : Text → Preserves
  , symbol : Text → Preserves
  , record : Preserves → List Preserves → Preserves
  , sequence : List Preserves → Preserves
  , set : List Preserves → Preserves
  , dictionary : List { mapKey : Preserves, mapValue : Preserves } → Preserves
  , embedded : Preserves → Preserves
  }

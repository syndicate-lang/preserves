#pragma once

#include <iostream>

#include "preserves.hpp"

namespace Preserves {
    template <typename T>
    class TextReader {
    public:
        TextReader(std::istream& i);
    };
}
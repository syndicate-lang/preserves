# Python Preserves

This package ([`preserves` on pypi.org](https://pypi.org/project/preserves/)) implements
[Preserves](https://preserves.dev/) for Python 3.x. It provides the core [semantics][] as well
as both the [human-readable text syntax](https://preserves.dev/preserves-text.html) (a superset
of JSON) and [machine-oriented binary format](https://preserves.dev/preserves-binary.html)
(including [canonicalization](https://preserves.dev/canonical-binary.html)) for Preserves. It
also implements [Preserves Schema](https://preserves.dev/preserves-schema.html) and [Preserves
Path](https://preserves.dev/preserves-path.html).

# Git repository

The project is [hosted on Gitlab](https://gitlab.com/preserves/preserves).

# Documentation

Documentation for the package is available at <https://preserves.dev/python/>.

# License

The package is licensed under the Apache License v2.0.

[semantics]: https://preserves.dev/preserves.html#semantics
